//
// Created by Daniel on 14/5/16.
//

#include <jni.h>
#include <stddef.h>

JNIEXPORT jstring JNICALL

Java_com_szaszdaniel_tutorial_androidndk_NDKWrapper_getMsgFromJni(JNIEnv *env, jobject instance) {

   return (*env)->NewStringUTF(env, "My first message from JNI !!!!");
}


Java_com_szaszdaniel_tutorial_androidndk_NDKWrapper_sayHelloFromJni(JNIEnv *env, jobject instance, jstring aName) {

    char *szResult;
    char *szFormat;
    char *szName;

    szFormat = "Hello from JNI : %s !!!";

    szResult = malloc(sizeof(szFormat) + 20);

    szName = (*env)->GetStringUTFChars(env, aName, NULL);

    sprintf(szResult, szFormat, szName);

    return (*env)->NewStringUTF(env, szResult);
}

