package com.szaszdaniel.tutorial.androidndk;

/**
 * Created by daniel on 14/5/16.
 */
public class NDKWrapper {

    static {
        System.loadLibrary("tutorial_android_ndk");
    }

    public native String getMsgFromJni();

    public native String sayHelloFromJni(String aName);
}
