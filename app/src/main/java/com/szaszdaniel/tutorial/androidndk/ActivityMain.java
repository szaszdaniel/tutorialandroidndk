package com.szaszdaniel.tutorial.androidndk;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityMain extends AppCompatActivity {

    private ApplicationTutorialNDK mApplicationTutorialNDK;
    private EditText mEditText;
    private Button mButton;
    private TextView mTextView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApplicationTutorialNDK = (ApplicationTutorialNDK) getApplication();


        setContentView(R.layout.activity_main);
        mEditText = (EditText)findViewById(R.id.editText);
        mButton = (Button) findViewById(R.id.button);
        mTextView = (TextView) findViewById(R.id.textView);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doCallSayHelloFromJNI();
            }
        });
    }

    private void doCallSayHelloFromJNI(){
        String stringName;
        String stringResult;

        stringName = mEditText.getText().toString();

        stringResult = mApplicationTutorialNDK.getNDKWrapper().sayHelloFromJni( stringName);

        mTextView.setText( stringResult);

    }
}
