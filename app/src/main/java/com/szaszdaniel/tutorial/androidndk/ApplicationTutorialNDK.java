package com.szaszdaniel.tutorial.androidndk;

import android.app.Application;

/**
 * Created by daniel on 14/5/16.
 */
public class ApplicationTutorialNDK extends Application {

    private NDKWrapper mNDKWrapper;

    @Override
    public void onCreate() {
        super.onCreate();

        mNDKWrapper = new NDKWrapper();
    }

    public NDKWrapper getNDKWrapper() {
        return mNDKWrapper;
    }
}
